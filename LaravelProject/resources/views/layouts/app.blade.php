<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Styles -->
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/design.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/jquery.dataTables.min.css')}}" rel='stylesheet'>
<link href='../css/fullcalendar.min.css' rel='stylesheet' />
<link href='../css/fullcalendar.print.min.css' rel='stylesheet' media='print' />
    <script src="js/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top sticky-top header-data">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand navbar-left" href="\home">
                        <div class="header-data">
                            <img src="/images/FAC-LOGO.png" style="width:50px;height:50px; background-color: gray; border-radius: 50%;">
                        </div>
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li class="header-data">
                                 <a href="{{ route('login') }}"><div class="header-data"><img style="height:30px; width:30px;" src="/images/icons/user.png" alt=""><br>Login</div></a>
                            </li>
                        @else
                            <li class="header-data">
                                <a href="\about"><div class="header-data"><img style="height:30px; width:30px;" src="/images/icons/about.png" alt=""><br>About Us</div></a>
                            </li>

                            <li class="header-data">
                                <a href="\gallery"><div class="header-data"><img style="height:30px; width:30px;" src="/images/icons/gallery.png" alt=""><br>Gallery</div></a>
                             </li>

                             <li class="header-data">

                                <a href="\plan"><div class="header-data"><img style="height:30px; width:30px;" src="/images/icons/plan.png" alt=""><br>Plan</div></a>
                             </li>

                            <li class="header-data">
                                <a href="\session"><div class="header-data"><img style="height:30px; width:30px;" src="/images/icons/class.png" alt=""><br>Class</div></a>
                             </li>

                             <li class="header-data">
                                <a href="\instructor"><div class="header-data"><img style="height:30px; width:30px;" src="/images/icons/instructor.png" alt=""><br>Instructor</div></a>
                             </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true"><div class="header-data">
                                    <img style=" border-radius:50%;height:50px; width:50px; background-color: #2ab27b;" src="/images/users/{{Auth::user()->photo}} " alt=""><span class="caret"><br></div></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a  href="\home">Home</a>
                                    </li>

                                    <li>
                                        <a  href="\profile\{{Auth::user()->id}}">Profile</a>
                                    </li>

                                    @if(Auth::User()->approved == 1)

                                     <li>
                                        <a  href="\vitalstat">Vital Status</a>
                                    </li>


                                    <li>
                                        <a  href="\task">History</a>
                                    </li>

                                     <li>
                                        <a  href="\schedule">Schedule</a>
                                    </li>
                                    @endif

                                    <hr>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        {{ csrf_field() }}

                                                    </form>
                                                    
                                                        Logout
                                        </a>
                                    </li>

                                    
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="../js/app.js"></script>
    <script src="../js/jquery-1.11.3.min.js"></script>
                    <script src="../js/jquery-1.11.3.min.js"></script>
                    <script src="../js/moment.min.js"></script>
                    <script src="../js/fullcalendar.min.js"></script>
</body>
</html>
