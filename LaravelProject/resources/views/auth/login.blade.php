<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cardio: Free One Page Template by Luka Cvetinovic</title>
    <meta name="description" content="Cardio is a free one page template made exclusively for Codrops by Luka Cvetinovic" />
    <meta name="keywords" content="html template, css, free, one page, gym, fitness, web design" />
    <meta name="author" content="Luka Cvetinovic for Codrops" />
    <!-- Favicons (created with http://realfavicongenerator.net/)-->
    <link rel="apple-touch-icon" sizes="57x57" href="img/favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/favicons/apple-touch-icon-60x60.png">
    <link rel="icon" type="image/png" href="img/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="img/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="img/favicons/manifest.json">
    <link rel="shortcut icon" href="img/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#00a8ff">
    <meta name="msapplication-config" content="img/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Normalize -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/normalize.css')}}">
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css')}}">
    <!-- Owl -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/owl.css')}}">
    <!-- Animate.css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/animate.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="{{ asset('fonts/font-awesome-4.1.0/css/font-awesome.min.css')}}">
    <!-- Elegant Icons -->
    <link rel="stylesheet" type="text/css" href="{{ asset('fonts/eleganticons/et-icons.css')}}">
    <!-- Main style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/cardio.css')}}">
</head>
<body id="page-top">

  
  <header class="masthead">
    <div class="container h-100">
      <div class="row h-100 align-items-center justify-content-center text-center">
        <div class="col-lg-10 align-self-end">
          <h1 class="text-uppercase text-white font-weight-bold"></h1>
          <hr class="divider my-4">
        </div>
        <div class="col-lg-8 align-self-baseline">
          
          @guest
            <div class="card">
                <div class="card-body">
                    
                <form class="form-horizontal" method="POST" action="{{ route('login') }}">

{{ csrf_field() }}

<div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">

        <input id="username" type="text" class="form-control" name="username" placeholder="Username" value="{{ old('username') }}" required autofocus>

        @if ($errors->has('username'))
            <span class="help-block">
                <strong>{{ $errors->first('username') }}</strong>
            </span>
        @endif
    </div>
<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

        <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>

        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
</div>
<div class="form-group">
    <div class="col-md-8 col-md-offset-4">
        <button type="submit" class="btn btn-success">
            Login
        </button>
    </div>
</div>

<br>
<hr>

<div class="form-group">
        <a class="register_link" href="{{ route('register') }}">Not a member? <br> <b>SIGN UP HERE</b></a>
</div>

</form>
                </div>
            </div>

            @else
            <div class="home-btn">
            <a href="/home">
                <img class="page-icon " src="/images/icons/home.png" alt="">
            </a>
            </div>
                <br>
                <hr>


            @endguest
        </div>
      </div>
    </div>
  </header>


    
</body>

</html>
