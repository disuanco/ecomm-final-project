@extends('layouts.app')

@section('title')
    Home
@endsection

@section('content')
<div class="container">
    <div class="row" style="height: 100%;">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading">Home</div>
                <div class="panel-body">

                    <div class="icon-display">
                        <img class="page-icon" src="/images/icons/home.png" alt="">
                    </div>
                    <div class="welcoming">HOME</div>

                    <hr>
                    <div class="items" style="font-size: 72px; color:#464646; background-color: darkgray;">WELCOME TO FIT N' CURVE!</div>
                    <hr>
                    
                     <div class="row icon-display" style="text-align: center;">
                        <div class="col-md-4" style="text-align: center;">
                        <a  href="\vitalstat"><img class="page-icon" src="/images/icons/vitalstat.png" alt="" style="width:150px;height:150px; background-color:#464646; border:0; border-radius: 0; box-shadow: none;"></a>
                        <br>
                        <div class="items">VITAL STATUS</div>
                        </div>
                        <div class="col-md-4" style="text-align: center;">
                        <a  href="\schedule"><img class="page-icon" src="/images/icons/schedule.png" alt="" style="width:150px;height:150px; background-color:#464646; border:0; border-radius: 0; box-shadow: none;">
                        <br>
                        <div class="items">GYM SCHEDULE</div>
                        </div>
                        <div class="col-md-4" style="text-align: center;">
                        <a  href="\task"><img class="page-icon" src="/images/icons/history.png" alt="" style="width:150px;height:150px; background-color:#464646; border:0; border-radius: 0; box-shadow: none;"></a>
                        <br>
                        <div class="items">TASK HISTORY</div>
                        </div>
                    </div>

                    <hr>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
