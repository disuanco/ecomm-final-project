<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class ProfileApiController extends Controller
{
    public function index()
    {      
      $users = User::all();
      return response()->json(compact('users'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $this->validate($request, [
              'fname' => 'required',
              'mname' => 'required',
              'lname' => 'required',
              'gender' => 'required',
              'dob' => 'required',
              'contact_no' => 'required',
              'email' => 'required',
              'password' => 'required',
              'address' => 'required',
              'username' => 'required',
              'photo' => 'required',
              'approved' => 'required'

            ]);
            $input = $request->all();
            User::create($input);
        return response()->json('success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $users = User::find($id);
      return response()->json(compact('users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
              'approved' => 'required'

            ]);
        $input = $request->all();

        $user = User::find($id);
        $user->update($input);
        return response()->json("Success");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return response()->json("Success");
    }}
