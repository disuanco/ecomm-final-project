<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fname', 'mname', 'lname', 'gender', 'dob', 'address', 'contact_no', 'email', 'password', 'username', 'approved','photo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    
    protected $dates = [
        'dob'
    ];
    public function setFirstNameAttribute($value){
        $this->attributes['fname']= ucfirst($value);
    }

     public function setMiddleNameAttribute($value){
        $this->attributes['mname']= ucfirst($value);
    }

     public function setLastNameAttribute($value){
        $this->attributes['lname']= ucfirst($value);
    }

     public function setPasswordAttribute($value){
        $this->attributes['password']= bcrypt($value);
    }

}
